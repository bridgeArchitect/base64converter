package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

const (
	/* constant row for transformation */
	rowBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	/* constant name for file of results */
	fileRes = "results.txt"
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to read row with printing of message */
func readRow(mess string, row *string) {

	/* declarations of variable */
	var (
		err error
	)

	/* print message and read row */
	fmt.Println(mess)
	_, err = fmt.Scanln(row)
	handleError(err)

}

/* function to read all file */
func readAllFile(filename string) []byte {

	/* declarations of variables */
	var (
		err      error
		file     *os.File
		symbols  []byte
	)

	/* open file */
	file, err = os.Open(filename)
	handleError(err)

	/* read file */
	symbols, err = ioutil.ReadAll(file)
	handleError(err)

	/* close file */
	err = file.Close()
	handleError(err)

	/* return answer */
	return symbols

}

/* function to convert byte array to base64 */
func convertBase64(symbols []byte) []byte {

	/* declarations of variables */
	var (
		i, i1 	  int
		number1   byte
		number2   byte
		number3   byte
		number4   byte
		byteArray []byte
		addition  int
	)

	/* create array for answer */
	if (len(symbols) * 4) % 3 == 0 {
		addition = 0
	} else if (len(symbols) * 4) % 3 == 1 {
		addition = 3
	} else if (len(symbols) * 4) % 3 == 2 {
		addition = 2
	}
	byteArray = make([]byte, len(symbols) * 4 / 3 + addition)

	/* go through all symbols */
	i1 = 0
	for i <= len(symbols) - 3 {

		/* convert main symbols to numbers */
		number1 = (symbols[i] & byte(252)) / byte(4)
		number2 = (symbols[i] & byte(3)) * byte(16) + (symbols[i + 1] & byte(240)) / byte(16)
		number3 = (symbols[i + 1] & byte(15)) * 4 + (symbols[i + 2] & byte(192)) / byte(64)
		number4 = symbols[i + 2] & byte(63)

		/* convert numbers to letters */
		byteArray[i1] = rowBase64[number1]
		byteArray[i1 + 1] = rowBase64[number2]
		byteArray[i1 + 2] = rowBase64[number3]
		byteArray[i1 + 3] = rowBase64[number4]

		i += 3
		i1 += 4

	}

	/* convert last symbols */
	if len(symbols) % 3 == 2 {

		/* if number of symbols executes such condition (n % 3 == 2) */
		number1 = (symbols[i] & byte(252)) / byte(4)
		number2 = (symbols[i] & byte(3)) * byte(16) + (symbols[i + 1] & byte(240)) / byte(16)
		number3 = (symbols[i + 1] & byte(15)) * 4

		byteArray[len(byteArray) - 4] = rowBase64[number1]
		byteArray[len(byteArray) - 3] = rowBase64[number2]
		byteArray[len(byteArray) - 2] = rowBase64[number3]
		byteArray[len(byteArray) - 1] = byte('=')

	} else if len(symbols) % 3 == 1 {

		/* if number of symbols executes such condition (n % 3 == 1) */
		number1 = (symbols[i] & byte(252)) / byte(4)
		number2 = (symbols[i] & byte(3)) * byte(16)

		byteArray[len(byteArray) - 4] = rowBase64[number1]
		byteArray[len(byteArray) - 3] = rowBase64[number2]
		byteArray[len(byteArray) - 2] = byte('=')
		byteArray[len(byteArray) - 1] = byte('=')

	}

	/* return answer */
	return byteArray

}

func writeFile(byteArray []byte) {

	/* declaration of variable */
	var (
		err        error     /* variable for error */
		fileOutput *os.File  /* file to write results */
	)

	/* create file to write results */
	fileOutput, err = os.Create(fileRes)
	handleError(err)

	/* write text */
	_, err = fileOutput.WriteString(string(byteArray))
	handleError(err)

	/* close file */
	err = fileOutput.Close()
	handleError(err)

}

func main () {

	/* declarations of variables */
	var (
		filename   string
		symbols    []byte
		byteArray  []byte
	)

	/* read filename */
	readRow("Name of file:", &filename)
	/* read symbols from file */
	symbols = readAllFile(filename)
	/* convert to base64 */
	byteArray = convertBase64(symbols)
	/* write answer to file */
	writeFile(byteArray)

}